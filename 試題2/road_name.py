import os
import json
import requests
import pandas as pd




"""
Get road names with API from Taiwan government opendata.
API query:
  1. YEAR: 民國年 (in path)
  2. COUNTY: 城市名
  3. PAGE: 分頁
"""
YEAR = 110
CITY = "新北市"

all_data = []
for page in range(1,10):
    url = f"https://www.ris.gov.tw/rs-opendata/api/v1/datastore/ODRP049/{YEAR}?COUNTY={CITY}&PAGE={page}"
    source = requests.get(url)
    source = json.loads(source.text, encoding="utf-8")
    message = source["responseMessage"]
    if message == "查無資料":
        print(f"結束於: page {page}")
        break
    
    data = source["responseData"]
    df = pd.DataFrame(data)
    all_data.append(df)
    print("PAGE: ", page)

road_df = pd.concat(all_data)
road_df = road_df.drop_duplicates()
print("Count: ", len(road_df))
print(road_df.head())

path = f"./data/road_name"
isExist = os.path.exists(path)
if not isExist:
  os.makedirs(path)

road_df.to_csv(f"./data/road_name/{CITY}.csv", index=False)
