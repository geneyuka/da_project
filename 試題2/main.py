import os
import re
import time
import pandas as pd

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

from twocaptcha import TwoCaptcha




# create selenium driver
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

# get all road names from New Taipei city
city = "新北市"
road_df = pd.read_csv(f"./data/road_name/{city}.csv")
dists = road_df[["site_id"]].drop_duplicates()
dists_list = list(dists.site_id)

for dist_name in dists_list: # dist loop
    path = f"./data/prod_data/{dist_name}"
    isExist = os.path.exists(path)
    if isExist:
        break
    else:
        os.makedirs(path)
    source_df = road_df.query(f"site_id == '{dist_name}'").reset_index(drop=True)
    data = []

    for num in range(len(source_df)): # road loop
        road_name = source_df.loc[num].road
        print(f"Index: {num}, 行政區: {dist_name}, 路段: {road_name}")

        """
        Step1: Open query page (https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3)
        Step2: Select district
        Step3: Input road name 
        Step4: Submit query
        Step5: Data collecting, loop all pages
        Breaking loop when:
            1. Get no result
            2. Finish last Page
            3. Website response error
        """
        # Step1
        driver.get('https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3')
        time.sleep(5)

        # Step2
        l = driver.find_element(By.NAME, "D1V")
        l.click()

        window_before = driver.window_handles[0]
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        
        content = driver.find_element(By.XPATH, f"//div[text()='{dist_name}']")
        content.click()

        # Step3
        driver.switch_to.window(window_before)
        road = driver.find_element(By.NAME, 'D3')
        road.send_keys(f"{road_name}")

        with open('./src/captcha.png', 'wb') as f:
            l = driver.find_element(By.ID, 'codeimg')
            f.write(l.screenshot_as_png)

        my_key = "c19b900048f90d658130d17321435f4e"

        config = {
                    'server':           '2captcha.com',
                    'apiKey':           f'{my_key}',
                    'softId':            123,
                    # 'callback':         'https://your.site/result-receiver',
                    'defaultTimeout':    120,
                    'recaptchaTimeout':  600,
                    'pollingInterval':   10,
                }

        solver = TwoCaptcha(**config)

        para = {'maxLength': 4}
        result = solver.normal('./src/captcha.png', param1=para)
        code = result['code']

        captcha = driver.find_element(By.NAME, 'Z1')
        captcha.send_keys(f"{code}")

        # Step4
        query = driver.find_element(By.XPATH, "//button[text()='查詢']")
        query.click()

        # Step5
        try:
            while True:
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//table[@class='responstable']/tfoot[@id='DataButton']/tr/td/b/a[3]")))
                soup = BeautifulSoup(driver.page_source)
                soup_table = soup.find_all("table")[-1]
                tables = pd.read_html(str(soup_table))
                table = tables[0]
                if len(table) == 1:
                    break
                page = table.iloc[-1]
                numbers = re.findall('[0-9]+', page[0])
                current_page = int(numbers[0])
                total_pages = int(numbers[1])
                print(f"current_page: {current_page}, last page: {total_pages}")

                table = table.iloc[:-1 , :]
                table["行政區"] = dist_name
                data.append(table)

                next_page = driver.find_element(By.XPATH, "//table[@class='responstable']/tfoot[@id='DataButton']/tr/td/b/a[3]")
                next_page.click()
                if current_page == total_pages:
                    break
        except:
            break
    if len(data) >= 1:
        data_df = pd.concat(data)
        data_df.to_parquet(f"{path}/{dist_name}.parquet.gzip", compression="gzip")

driver.quit()