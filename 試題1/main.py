import time

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

from twocaptcha import TwoCaptcha




# create selenium driver
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
driver.get('http://www.goldenjade.com.tw/captchaCheck/check/imgcheck_form.html')

with open('./src/captcha.png', 'wb') as f:
    l = driver.find_element(By.ID, 'rand-img')
    f.write(l.screenshot_as_png)

# create captcha solver
my_key = "c19b900048f90d658130d17321435f4e"
config = {
            'server':           '2captcha.com',
            'apiKey':           f'{my_key}',
            'softId':            123,
            'defaultTimeout':    120,
            'recaptchaTimeout':  600,
            'pollingInterval':   10,
        }
solver = TwoCaptcha(**config)
para = {'maxLength': 6}
result = solver.normal('./src/captcha.png', param1=para)
code = result['code']

# selenium actions
"""
Step1: send captcha code
Step2: submit
Step3: accept alert
"""
driver.find_element(By.NAME, 'checknum').send_keys(f"{code}")

content = driver.find_element(By.XPATH, f"//input[@type='submit']")
content.click()

WebDriverWait(driver, 5).until(EC.alert_is_present())
driver.switch_to.alert.accept()

WebDriverWait(driver, 5).until(EC.alert_is_present())
driver.switch_to.alert.accept()

time.sleep(10)
driver.quit()