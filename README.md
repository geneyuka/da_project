# DA_Project
Project Author: Gene Chang
- geneyuka@gmail.com

## Environment
```
Mac OS Monterey 12.4
python 3.7.2
pip3 18.1
```

## Before started
```
pip3 install -r requirements.txt
```

## Getting started
試題1: 請透過程式破解下列測試網站驗證碼機制(網頁爬蟲技能驗測) 
- http://www.goldenjade.com.tw/captchaCheck/check/imgcheck_form.html
```
cd 試題1
python3 main.py
```
試題2: 請透過程式爬取新北市各區路段上的使用執照資料
- 全國路名資料: https://data.gov.tw/dataset/35321
- https://building-management.publicwork.ntpc.gov.tw/bm_query.jsp?rt=3
```
cd 試題2
python3 road_name.py
python3 main.py
```
試題3: 請透過程式將爬取下來的使用執照整理並儲存，分析並回答下列問題:
- 新北市哪一區發照數量最多、最少?
- 請將各發照日期距離 2020/11/1 之天數算出(稱:發照期間)，新北市哪一區有最長發照期間的建物?該建物起造人、設計人是否在新北市也有其他相應建物?
- 假如我們定義:發照期間越久，代表建物越老舊、可能發生電線走火的機率高，請由發照期間推斷，協助核保分析新北市哪一區電線走火的風險最高?為什麼?(提示:需考慮發照期間的分佈)
```
cd 試題3
python3 main.py
- 答案產出在此路徑: 試題3/result/answer.txt
```