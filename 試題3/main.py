import pandas as pd

from datetime import datetime




def date_transfer(x):
    """
    Calculate n days from 2020-11-01
    """
    if x == 'NA':
        return '2020-11-01'
    ele = x.split("/")
    if len(ele) == 3:
        year = str(int(ele[0])+1911)
        month = ele[1]
        day = ele[2]
        if year >= '1946' and month in ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']:
            return year + '-' + month + '-' + day
        else:
            return '2020-11-01'
    else:
        return '2020-11-01'

# load data
df = pd.read_parquet("../試題2/data/prod_data")
df = df.fillna("NA")
row_count = len(df)

# Q1 發照數量
q1 = pd.DataFrame(df.groupby(["行政區"]).size().sort_values(ascending=False)).reset_index()
q1.columns = ["dist", "cnt"]
first_row = q1.iloc[0].tolist()
last_row = q1.iloc[-1].tolist()
max_dist = first_row[0]
max_cnt = first_row[1]
min_dist = last_row[0]
min_cnt = last_row[1]


# Q2 計算發照時間
target_date = "2020-11-01"
target_date = datetime.strptime(target_date, '%Y-%m-%d')

df["license_date"] = df.發照日期.apply(date_transfer)
df['license_date'] = pd.to_datetime(df['license_date'], format='%Y-%m-%d')
df['diff_n_days'] = df.license_date.apply(lambda x: (x-target_date).days)
df = df.sort_values(by="diff_n_days", ascending=True, axis=0)
early_row = df.iloc[0].tolist()
early_dist = early_row[6]
early_year = early_row[5]
early_builder = early_row[2].split("法代")[1]
early_designer = early_row[3]

A = df.query(f"設計人 == '{early_designer}'")
B = A.loc[A['起造人'].str.contains("何連順", case=False)]
if len(B) >= 2:
    q2_2 = "有其他相應建物"
else:
    q2_2 = "沒有其他相應建物"

# Q3
q3_df = pd.DataFrame(df.groupby(["行政區"])['diff_n_days'].median().sort_values(ascending=True))
risky_row = q3_df.iloc[0]
risky_dist = risky_row.name

ans_str = f"""資料筆數: {row_count}

Q1: 發照數量
最多: {max_dist}, {max_cnt}
最少: {min_dist}, {min_cnt}

Q2-1: 最長發照期間的建物?
區域: {early_dist}
年份: {early_year}
起造人: {early_builder}
設計人: {early_designer}

Q2-2: 該建物起造人、設計人是否在新北市也有其他相應建物?
{q2_2}

Q3:
風險最高區域為: {risky_dist}
原因: 將每個行政區的發照期間進行排序，並計算中位數。代表50%以上的建物發照期間超過中位數。
因發照期間越久，代表建物越老舊、可能發生電線走火的機率高，且發照期間天數為負數。
所以電線走火最高風險區域為發照期間中位數最小者。
"""
print(ans_str)
with open("./result/answer.txt", 'w', encoding='utf-8') as file:
    file.write(ans_str)

df.to_csv("./result/data.csv", index=False, encoding='utf_8_sig')